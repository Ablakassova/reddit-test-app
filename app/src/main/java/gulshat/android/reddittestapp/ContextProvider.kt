package gulshat.android.reddittestapp

import kotlin.coroutines.CoroutineContext

interface ContextProvider {
    val ui: CoroutineContext
}