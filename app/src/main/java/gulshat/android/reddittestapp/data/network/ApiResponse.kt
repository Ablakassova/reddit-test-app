package gulshat.android.reddittestapp.data.network

abstract class RedditResponse<T>

data class ApiResponse<T>(
    val data: T?,
    val kind: Kind?
)

data class RedditListingResponse<T>(
    val modhash: String?,
    val dist: Int?,
    val children: List<ApiResponse<T>>?,
    val after: String?,
    val before: String?,
    val error: String?
) : RedditResponse<T>()

data class RedditListing<T>(
    val list: List<T>?,
    val after: String?
)

enum class Kind(val value: String) {

    List("Listing"),
    Other("Other")
}