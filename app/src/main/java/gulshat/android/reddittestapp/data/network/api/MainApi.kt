package gulshat.android.reddittestapp.data.network.api

import gulshat.android.reddittestapp.data.network.ApiResponse
import gulshat.android.reddittestapp.data.network.RedditListingResponse
import gulshat.android.reddittestapp.domain.entities.Post
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface MainApi {

    @GET("gaming/top.json")
    fun getGamingTopList(@Query("after") after: String?, @Query("limit") limit: Int): Deferred<ApiResponse<RedditListingResponse<Post>>>
}