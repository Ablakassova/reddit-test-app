package gulshat.android.reddittestapp.data.network

import android.accounts.NetworkErrorException
import retrofit2.HttpException
import java.io.IOException
import java.lang.Exception
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

sealed class Response<out R> {

    data class Success<out T>(val data: T) : Response<T>()

    data class Error(
        val exception: Exception? = null, val apiErrorMessage: String? = null
    ) : Response<Nothing>() {

        val isNetworkError = exception is NetworkErrorException
                || exception is SocketTimeoutException
                || exception is UnknownHostException
                || exception is ConnectException
                || exception is IOException

        val isHttpException = exception is HttpException
    }

    object Loading : Response<Nothing>()
}