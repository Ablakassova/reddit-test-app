package gulshat.android.reddittestapp.data.repository

import gulshat.android.reddittestapp.data.network.RedditListing
import gulshat.android.reddittestapp.data.network.Response
import gulshat.android.reddittestapp.domain.entities.Post

interface MainRepository {

    suspend fun getGamingTopList(limit: Int, after: String?): Response<RedditListing<Post>>
}