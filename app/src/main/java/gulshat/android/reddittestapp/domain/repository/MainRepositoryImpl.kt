package gulshat.android.reddittestapp.domain.repository

import gulshat.android.reddittestapp.data.network.RedditListing
import gulshat.android.reddittestapp.data.network.Response
import gulshat.android.reddittestapp.data.network.api.MainApi
import gulshat.android.reddittestapp.data.repository.MainRepository
import gulshat.android.reddittestapp.domain.entities.Post
import java.lang.Exception
import javax.inject.Inject

class MainRepositoryImpl @Inject constructor(
    private val mainApi: MainApi
) : MainRepository {

    override suspend fun getGamingTopList(
        limit: Int,
        after: String?
    ): Response<RedditListing<Post>> {
        return try {
            val response = mainApi.getGamingTopList(after, limit).await().data
            if (response?.error == null && response?.children != null) {
                Response.Success(
                    RedditListing(
                        response.children.map { it.data as Post },
                        response.after
                    )
                )
            } else {
                Response.Error(apiErrorMessage = response?.error)
            }
        } catch (exception: Exception) {
            Response.Error(exception)
        }
    }
}