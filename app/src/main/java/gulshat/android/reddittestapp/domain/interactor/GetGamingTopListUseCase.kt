package gulshat.android.reddittestapp.domain.interactor

import gulshat.android.reddittestapp.data.network.RedditListing
import gulshat.android.reddittestapp.data.network.Response
import gulshat.android.reddittestapp.domain.entities.Post
import gulshat.android.reddittestapp.domain.repository.MainRepositoryImpl
import javax.inject.Inject

class GetGamingTopListUseCase @Inject constructor(
    private val mainRepository: MainRepositoryImpl
) : UseCase<GetGamingTopListUseCaseParams, Response<RedditListing<Post>>>() {

    override suspend fun execute(params: GetGamingTopListUseCaseParams): Response<RedditListing<Post>> {
        return mainRepository.getGamingTopList(params.limit, params.after)
    }
}

data class GetGamingTopListUseCaseParams(
    val limit: Int,
    val after: String?
)