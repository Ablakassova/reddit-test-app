package gulshat.android.reddittestapp.domain.interactor

abstract class UseCase<in Params, Response> {

    abstract suspend fun execute(params: Params): Response

    suspend operator fun invoke(params: Params): Response = execute(params)
}