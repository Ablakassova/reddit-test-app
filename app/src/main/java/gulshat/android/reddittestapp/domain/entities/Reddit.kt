package gulshat.android.reddittestapp.domain.entities

import com.google.gson.annotations.SerializedName

data class Post(
    val subreddit: SubReddit?,
    val ups: Long?,
    val downs: Long?,
    val title: String?,
    val permalink: String?
) {

    fun areContentsTheSame(newPost: Post): Boolean {
        return this.title == newPost.title
                && this.subreddit == newPost.subreddit
                && this.downs == newPost.downs
                && this.ups == newPost.ups
    }
}

enum class SubReddit {

    @SerializedName("gaming")
    GAMING,
    @SerializedName("gifs")
    GIFS,
    @SerializedName("other")
    OTHER
}