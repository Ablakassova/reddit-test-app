package gulshat.android.reddittestapp.di.module.fragments

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import gulshat.android.reddittestapp.di.viewModel.ViewModelKey
import gulshat.android.reddittestapp.ui.main.MainViewModel

@Module
abstract class MainModule {
}


@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun mainPage(viewModel: MainViewModel): ViewModel

}