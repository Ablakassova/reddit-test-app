package gulshat.android.reddittestapp.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import gulshat.android.reddittestapp.App
import gulshat.android.reddittestapp.di.viewModel.ContextProviderImpl
import gulshat.android.reddittestapp.ContextProvider
import javax.inject.Singleton


@Module(
    includes = [
        DataModule::class,
        ViewModelModule::class
    ]
)
class ApplicationModule {

    @Provides
    @Singleton
    internal fun provideApplicationContext(app: App): Context {
        return app
    }

    @Provides
    @Singleton
    internal fun provideContextProvider(contextProviderImpl: ContextProviderImpl): ContextProvider =
        contextProviderImpl
}