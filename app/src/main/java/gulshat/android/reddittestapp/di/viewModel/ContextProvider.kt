package gulshat.android.reddittestapp.di.viewModel

import gulshat.android.reddittestapp.ContextProvider
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

class ContextProviderImpl @Inject constructor() :
    ContextProvider {

    override val ui: CoroutineContext
        get() = Dispatchers.Main
}