package gulshat.android.reddittestapp.di.module

import dagger.Binds
import dagger.Module
import gulshat.android.reddittestapp.data.repository.MainRepository
import gulshat.android.reddittestapp.domain.repository.MainRepositoryImpl

@Module
abstract class DataModule {

    @Binds
    abstract fun main(mainRepositoryImpl: MainRepositoryImpl): MainRepository
}