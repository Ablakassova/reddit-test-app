package gulshat.android.reddittestapp.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import gulshat.android.reddittestapp.ui.main.MainActivity

@Module
abstract class ActivityBindingModule {

    @ContributesAndroidInjector
    abstract fun bindRoot(): MainActivity
}