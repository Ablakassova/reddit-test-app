package gulshat.android.reddittestapp.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import gulshat.android.reddittestapp.di.module.fragments.MainViewModelModule
import gulshat.android.reddittestapp.di.viewModel.ViewModelFactory

@Module(includes = [MainViewModelModule::class])
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}