package gulshat.android.reddittestapp

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancelChildren
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel(
    private val contextProvider: ContextProvider
) : ViewModel(), CoroutineScope {

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = contextProvider.ui + job

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    fun cancel() {
        coroutineContext.cancelChildren()
    }
}