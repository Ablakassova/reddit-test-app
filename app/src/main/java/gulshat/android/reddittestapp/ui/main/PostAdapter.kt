package gulshat.android.reddittestapp.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import gulshat.android.reddittestapp.R
import gulshat.android.reddittestapp.domain.entities.Post
import kotlinx.android.synthetic.main.item_post.view.*

class PostAdapter(
    private val onClickListener: (Post) -> Unit,
    private val bottomReachListener: (Unit) -> Unit
) : ListAdapter<Post, ViewHolder>(
    PostDiffUtil
) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false),
            onClickListener
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (position == itemCount - 1) {
            bottomReachListener(Unit)
        }
        holder.bind(getItem(position))
    }
}

class ViewHolder(
    view: View,
    private val onItemClickListener: (Post) -> Unit
) : RecyclerView.ViewHolder(view) {

    private var post: Post? = null

    init {
        itemView.setOnClickListener {
            post?.let {
                onItemClickListener(it)
            }
        }
    }

    fun bind(post: Post) {
        this.post = post
        itemView.apply {
            titleTextView.text = post.title
            subredditTextView.text = post.subreddit?.name
            upVotesTextView.text = post.ups.toString()
            downVotesTextView.text = post.downs.toString()
        }
    }
}

object PostDiffUtil : DiffUtil.ItemCallback<Post>() {
    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.permalink == newItem.permalink
    }

    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.areContentsTheSame(newItem)
    }
}