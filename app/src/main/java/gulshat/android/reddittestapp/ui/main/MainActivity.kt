package gulshat.android.reddittestapp.ui.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.ProgressBar
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import gulshat.android.reddittestapp.R
import gulshat.android.reddittestapp.data.network.Response
import gulshat.android.reddittestapp.domain.entities.Post
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val mainViewModel: MainViewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(MainViewModel::class.java)
    }

    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObservables()
        initView()
        mainViewModel.getGamingTopList()
    }

    override fun onRefresh() {
        recyclerView.adapter = PostAdapter(onItemClickListener, bottomReachListener)
        mainViewModel.getGamingTopList(forceUpdate = true)
    }

    private fun onError(error: Response.Error) {
        val message = error.apiErrorMessage
            ?: getString(
                when {
                    error.isNetworkError -> R.string.network_error
                    error.isHttpException -> R.string.http_error
                    else -> R.string.unknown_error
                }
            )
        Snackbar.make(container, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun initObservables() {
        mainViewModel.gamingTopListLiveData.observe(this, Observer {
            handleLoading()
            when (it) {
                is Response.Success -> {
                    (recyclerView.adapter as PostAdapter).submitList(it.data)
                }
                is Response.Error -> {
                    onError(it)
                }
            }
        })
    }

    private fun initView() {
        val verticalDividerItemDecoration = DividerItemDecoration(
            recyclerView.context,
            LinearLayoutManager.VERTICAL
        )
        recyclerView.apply {
            adapter = PostAdapter(onItemClickListener, bottomReachListener)
            addItemDecoration(verticalDividerItemDecoration)
        }
        swipeRefreshLayout.setOnRefreshListener(this)
        progressBar = centerProgressBar
    }

    private fun handleLoading() {
        if (progressBar.isVisible) progressBar.isGone = true
        else if (swipeRefreshLayout.isRefreshing) swipeRefreshLayout.isRefreshing = false
    }

    private val onItemClickListener: (Post) -> Unit = {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("$BASE_URL${it.permalink}"))
        startActivity(intent)
    }

    private val bottomReachListener: (Unit) -> Unit = {
        mainViewModel.getGamingTopList()
        progressBar = bottomProgressBar.apply {
            isVisible = true
        }
    }

    companion object {
        private const val BASE_URL = "https://www.reddit.com"
    }
}
