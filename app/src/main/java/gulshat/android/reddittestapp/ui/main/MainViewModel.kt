package gulshat.android.reddittestapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import gulshat.android.reddittestapp.BaseViewModel
import gulshat.android.reddittestapp.ContextProvider
import gulshat.android.reddittestapp.data.network.Response
import gulshat.android.reddittestapp.domain.entities.Post
import gulshat.android.reddittestapp.domain.interactor.GetGamingTopListUseCase
import gulshat.android.reddittestapp.domain.interactor.GetGamingTopListUseCaseParams
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getGamingTopListUseCase: GetGamingTopListUseCase,
    contextProvider: ContextProvider
) : BaseViewModel(contextProvider) {

    private val gamingTopList = mutableListOf<Post>()

    private var after: String? = null

    private var gamingTopListMutableLiveData = MutableLiveData<Response<List<Post>?>>()

    val gamingTopListLiveData: LiveData<Response<List<Post>?>>
        get() = gamingTopListMutableLiveData

    fun getGamingTopList(limit: Int = 25, forceUpdate: Boolean = false) {
        if (forceUpdate) {
            gamingTopList.clear()
            after = null
        }
        launch {
            val response = getGamingTopListUseCase(GetGamingTopListUseCaseParams(limit, after))
            gamingTopListMutableLiveData.value = if (response is Response.Success) {
                gamingTopList.addAll(response.data.list as MutableList)
                after = response.data.after
                Response.Success(mutableListOf<Post>().apply {
                    addAll(gamingTopList)
                })
            } else {
                response as Response.Error
            }
        }
    }
}